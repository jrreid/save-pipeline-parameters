A sample project showing how [prefilled variables](https://docs.gitlab.com/ee/ci/pipelines/index.html#prefill-variables-in-manual-pipelines) can be stored/persisted as a record of "pipeline context".

This sample project [defines](https://gitlab.com/jrreid/save-pipeline-parameters/-/blob/main/.gitlab-ci.yml?ref_type=heads#L1-16) two CI variables as prefilled, with a drop-down list of options.

This will present a list of options when manually triggering the pipeline:
![pipeline options](images/opt.png)

The `print-env-job` uses `env`, `grep`, `jq` and `tee` to both output in its job log, as well as write, to either an environment file, or a json-formatted file, the values of the environment variables containing `OPT_` in the name.

**Output in the job log:**
![job log output](images/log.png
)

**Persisted as artifacts:**
![job artifacts](images/artifact.png)

The job log, or job artifacts can be inspected after pipeline execution to understand what the pipeline variables were set to upon triggering.


**NOTE:** While manual pipeline variables are the second-highest in the [order of variable precedence](https://docs.gitlab.com/ee/ci/variables/#cicd-variable-precedence), it is possible that the value of these variables is overridden in an individual job, or by a scan execution policy.
